# DARPA CriticalMAAS Project

**Release Description: Attention U-net model inference**  

---

**Usage:**

To use the Map Inference Tool, run the script with the necessary arguments:
```
python inference.py --mapPath "/path/to/your/map.hdf5" --jsonPath "/path/to/your/legends.json" --featureType Polygon --outputPath "/path/to/output/directory" --modelPath "/path/to/your/model.h5"
```

**Dependencies:**
- Python Libraries: argparse, math, os, numpy, rasterio, tensorflow, keras
- Custom Modules: data_util, h5Image, unet_util

**Future Enhancements:**
- Integration of georeferencing capabilities for output TIFF images.
