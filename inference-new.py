import os
import cv2
import rasterio
import numpy as np
from PIL import Image
import tensorflow as tf
from data_util import DataLoader
# import segmentation_models as sm
# from keras.models import load_model
from tensorflow.keras.models import load_model
from unet_util import dice_coef_loss, dice_coef, jacard_coef, dice_coef_loss, Residual_CNN_block, multiplication, attention_up_and_concatenate, multiplication2, attention_up_and_concatenate2, UNET_224, evaluate_prediction_result

# Set the limit to a larger value than the default
Image.MAX_IMAGE_PIXELS = 200000000  # For example, allow up to 200 million pixels

def inference(map_file_name, prediction_path, model):
    """
    Load map image, find corresponding legend images, create inputs, predict, and reconstruct images.

    Parameters:
    map_file_name (str): Name of the map image file (e.g., 'AR_Maumee.tif').
    prediction_path (str): Path to save the predicted image.
    """
    # Set the paths
    map_dir = '/projects/bbym/shared/data/cma/validation/'
    map_img_path = os.path.join(map_dir, map_file_name)
    json_file_name = os.path.splitext(map_file_name)[0] + '.json'
    json_file_path = os.path.join(map_dir, json_file_name)

    patch_size=(256, 256, 3)
    overlap=30

    # Instantiate DataLoader and get processed data
    data_loader = DataLoader(map_img_path, json_file_path, patch_size, overlap)
    processed_data = data_loader.get_processed_data()
    
    map_patches = processed_data['map_patches']
    total_row, total_col, _, _, _, _ = map_patches.shape
    
    map_patches_reshape = map_patches.reshape(-1, *patch_size)
    
    # 'poly_legends', 'pt_legends', 'line_legends'
    for legend in ['poly_legends']:
        for legend_img, legend_label in processed_data[legend]:
            
            legend_patches = np.repeat(legend_img[np.newaxis],total_row*total_col, axis = 0)

            data_input = tf.concat(axis=3, values=[map_patches_reshape, legend_patches])
            
            predicted_patches = model.predict(data_input, verbose = 0)
            
            predicted_patches = np.where(predicted_patches >= 0.5, 1, 0).astype(np.uint8)
            
            predicted_patches_reshape = predicted_patches.reshape(total_row, total_col, patch_size[0], patch_size[1])
            
            reconstructed_image = data_loader.reconstruct_data(predicted_patches_reshape)

            output_image_path = os.path.join(prediction_path, f"{os.path.splitext(map_file_name)[0]}_{legend_label}.tif")

            with rasterio.open(map_img_path) as src:
                metadata = src.meta

            metadata.update({
                'dtype': 'uint8',
                'count': 1,
                'height': reconstructed_image.shape[0],
                'width': reconstructed_image.shape[1],
                'compress': 'lzw',
            })

            with rasterio.open(output_image_path, 'w', **metadata) as dst:
                dst.write(reconstructed_image, 1)

            print(f"Predicted image saved at: {output_image_path}")

################################################################
##### Prepare the model configurations #########################
################################################################
name_id = 'invalid_patch' #You can change the id for each run so that all models and stats are saved separately.
prediction_path = '/projects/bbym/nathanj/attentionUnet/predicts_'+name_id+'/'
model_path = '/projects/bbym/nathanj/attentionUnet/models_'+name_id+'/'

# Avaiable backbones for Unet architechture
# 'vgg16' 'vgg19' 'resnet18' 'resnet34' 'resnet50' 'resnet101' 'resnet152' 'inceptionv3'
# 'inceptionresnetv2' 'densenet121' 'densenet169' 'densenet201' 'seresnet18' 'seresnet34'
# 'seresnet50' 'seresnet101' 'seresnet152', and 'attentionUnet'
backend = 'attentionUnet'
name = 'Unet-'+ backend

finetune = False
if (finetune): name += "_ft"

model = load_model(model_path+name+'.h5',
                    custom_objects={'multiplication': multiplication,
                                'multiplication2': multiplication2,
                                'dice_coef_loss':dice_coef_loss,
                                'dice_coef':dice_coef,})

# Example of how to use the function
inference('AR_StJoe.tif', prediction_path, model)