import argparse
import math
import cv2
import os
import time
import numpy as np
from PIL import Image
import rasterio
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from keras.models import load_model
import matplotlib.gridspec as gridspec
from data_util import DataLoader
from h5Image import H5Image
from unet_util import (UNET_224, Residual_CNN_block,
                        attention_up_and_concatenate,
                        attention_up_and_concatenate2, dice_coef,
                        dice_coef_loss, evaluate_prediction_result, jacard_coef,
                        multiplication, multiplication2)

import logging

"""
# Example file paths and legends (adjust these to your actual file paths and legends)
images_paths = ["path/to/image1.tiff", "path/to/image2.tiff"]
legends = [{'legend1': legend1_patch, 'legend2': legend2_patch}, {'legend1': legend1_patch, 'legend2': legend2_patch}]
checkpoint_path = "path/to/model/checkpoint.h5"

# Call the inference function
results = inference(images_paths, legends, checkpoint_path, featureType='Polygon')
"""

logger = logging.getLogger('primordial-positron')

def prediction_mask(prediction_result, map_array):
    """
    Apply a mask to the prediction image to isolate the area of interest.

    Parameters:
    - prediction_result: numpy array, The output of the model after prediction.
    - map_name: str, The name of the map used for prediction.

    Returns:
    - masked_img: numpy array, The masked prediction image.
    """
    logger.info("Starting prediction mask application.")

    # Convert the RGB map array to grayscale for further processing
    gray = cv2.cvtColor(map_array, cv2.COLOR_BGR2GRAY)

    # Identify the most frequent pixel value, which will be used as the background pixel value
    pix_hist = cv2.calcHist([gray],[0],None,[256],[0,256])
    background_pix_value = np.argmax(pix_hist, axis=None)

    # Flood fill from the corners to identify and modify the background regions
    height, width = gray.shape[:2]
    corners = [[0,0],[0,height-1],[width-1, 0],[width-1, height-1]]
    for c in corners:
        cv2.floodFill(gray, None, (c[0],c[1]), 255)

    # Adaptive thresholding to remove small noise and artifacts
    thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 21, 4)

    # Detect edges using the Canny edge detection method
    thresh_blur = cv2.GaussianBlur(thresh, (11, 11), 0)
    canny = cv2.Canny(thresh_blur, 0, 200)
    canny_dilate = cv2.dilate(canny, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7)))

    # Detect contours in the edge-detected image
    contours, hierarchy = cv2.findContours(canny_dilate, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    # Retain only the largest contour
    contour = sorted(contours, key=cv2.contourArea, reverse=True)[0]
    
    # Create an empty mask of the same size as the prediction_result
    wid, hight = prediction_result.shape[0], prediction_result.shape[1]
    mask = np.zeros([wid, hight])
    mask = cv2.fillPoly(mask, pts=[contour], color=(1)).astype(np.uint8)

    # Convert prediction result to a binary format using a threshold
    prediction_result_int = (prediction_result > 0.5).astype(np.uint8)

    # Apply the mask to the thresholded prediction result
    masked_img = cv2.bitwise_and(prediction_result_int, mask)

    logger.info("Completed prediction mask application.")
    return masked_img

def pad_image(image, patch_size):
    """
    Pads the image so that its dimensions are divisible by the patch size.

    Parameters:
    - image: numpy array, The input image.
    - patch_size: int, The size of each patch.

    Returns:
    - padded_image: numpy array, The padded image.
    """
    logger.info(f"Padding image with patch size {patch_size}.")
    pad_height = patch_size - image.shape[0] % patch_size
    pad_width = patch_size - image.shape[1] % patch_size
    pad_height = pad_height if pad_height != patch_size else 0
    pad_width = pad_width if pad_width != patch_size else 0
    padded_image = np.pad(image, ((0, pad_height), (0, pad_width), (0, 0)), mode='constant')
    logger.info("Image padding completed.")
    return padded_image

def extract_patches(image, patch_size):
    """
    Extracts patches from an image with padding if necessary.

    Parameters:
    - image: numpy array, The input image.
    - patch_size: int, The size of each patch.

    Returns:
    - patches: numpy array, The extracted patches.
    - patch_locations: list, The top-left corner locations of each patch.
    """
    logger.info("Extracting patches from the image.")
    padded_image = pad_image(image, patch_size)
    patches = []
    patch_locations = []
    for i in range(0, padded_image.shape[0], patch_size):
        for j in range(0, padded_image.shape[1], patch_size):
            patch = padded_image[i:i+patch_size, j:j+patch_size]
            patches.append(patch)
            patch_locations.append((i, j))
    logger.info(f"Extracted {len(patches)} patches.")
    return np.array(patches), patch_locations, padded_image.shape

def stitch_patches(predictions, patch_locations, original_image_shape, padded_image_shape, patch_size):
    """
    Stitches patches back into a full image and crops it to the original image size.

    Parameters:
    - predictions: numpy array, The predictions for each patch.
    - patch_locations: list, The top-left corner locations of each patch.
    - original_image_shape: tuple, The shape of the original image.
    - padded_image_shape: tuple, The shape of the padded image.
    - patch_size: int, The size of each patch.

    Returns:
    - full_prediction: numpy array, The stitched prediction for the full image.
    """
    logger.info("Starting to stitch patches.")
    full_prediction_padded = np.zeros(padded_image_shape[:2])
    for pred, loc in zip(predictions, patch_locations):
        i, j = loc
        full_prediction_padded[i:i+patch_size, j:j+patch_size] = pred[:patch_size, :patch_size]

    # Crop the padded prediction to match the original image size
    full_prediction = full_prediction_padded[:original_image_shape[0], :original_image_shape[1]]
    logger.info("Stitching of patches completed.")
    return full_prediction

def save_prediction_to_tiff(prediction, file_path, image_file_name):
    """
    Save the prediction result to a TIFF file.

    Parameters:
    - prediction: numpy array, The prediction result to be saved.
    - file_path: str, The file path where the TIFF file will be saved.
    - transform: rasterio Affine Transform, optional, Geospatial transform of the prediction (default: None).
    - crs: str, optional, Coordinate Reference System of the prediction (default: None).
    """

    prediction_image = (prediction*255).astype(np.uint8)
    prediction_image = np.expand_dims(prediction_image, axis=0)

    with rasterio.open(image_file_name) as src_dataset:

        # Get a copy of the source dataset's profile. Thus our
        # destination dataset will have the same dimensions,
        # number of bands, data type, and georeferencing as the
        # source dataset.

        kwds = src_dataset.profile

        # Change the format driver for the destination dataset to
        # 'GTiff', short for GeoTIFF.
        kwds['driver'] = 'GTiff'

        # Add GeoTIFF-specific keyword arguments.
        kwds['dtype'] = prediction_image.dtype
        kwds['height'] = prediction_image.shape[1]
        kwds['width'] = prediction_image.shape[2]
        kwds['compress'] = 'lzw'

        with rasterio.open(file_path, 'w', **kwds) as dst:
            dst.write(prediction, 1)


    logger.info(f"Saved prediction to {file_path}")





def inference_image(image, image_file_name, legends, model, feature_type, patch_size=256, save_dir='predictions'):
    """
    Perform inference on an image using a trained model and extract predictions for each legend.

    Parameters:
    - image: numpy array, The image on which to perform inference.
    - image_file_name: str, The file name of the image.
    - legends: dict, A dictionary where keys are legend names and values are corresponding legend patches.
    - model: tensorflow.keras Model, The trained deep learning model for inference.
    - feature_type: str, The type of features to filter from the legends ('Polygon', 'Point', 'Line', or 'All').
    - patch_size: int, optional, The size of the patches to be extracted from the image for batch processing.
    - save_dir: str, optional, The directory where prediction TIFF files will be saved.

    Returns:
    - predictions: dict, A dictionary where keys are legend names and values are the masked prediction results for each legend.
    """
    predictions = {}
    map_legends = [legend for legend in legends.keys() if "_" + feature_type.lower() in legend] if feature_type != "All" else legends.keys()
    original_image_shape = image.shape
    image_patches, patch_locations, padded_image_shape = extract_patches(image, patch_size)

    for legend in map_legends:
        legend_patch = legends[legend]
        prediction_batch = model.predict([image_patches, np.tile(legend_patch, (len(image_patches), 1, 1, 1))], verbose=0)
        full_prediction = stitch_patches(prediction_batch, patch_locations, original_image_shape, padded_image_shape, patch_size)
        masked_prediction = prediction_mask(full_prediction, image)

        # Construct the file path using the image file name and legend
        base_name = os.path.splitext(os.path.basename(image_file_name))[0]
        file_path = os.path.join(save_dir, f"{base_name}_{legend}.tiff")

        # Save the prediction as a TIFF file
        save_prediction_to_tiff(masked_prediction, file_path, image_file_name)

        predictions[legend] = masked_prediction

    return predictions


def inference(images_paths, legends, checkpoint, **kwargs):
    """
    Perform inference on a list of images using a specific model checkpoint. 
    The function handles loading the model with appropriate custom objects 
    and processes each image using the `inference_image` function.

    Parameters:
    - images: list, A list of numpy arrays representing the images to process.
    - legends: list, A list of dictionaries corresponding to each image, where each dictionary contains legend information.
    - checkpoint: str, The path to the model checkpoint file.
    - kwargs: dict, optional, Additional keyword arguments. Currently supports 'featureType' which can be 'Polygon', 'Point', or 'All'.

    Returns:
    - outputs: list, A list of dictionaries, where each dictionary contains the masked prediction results for each legend in the corresponding image.
    """
    logger.info("Starting batch inference.")
    featureType = kwargs.get('featureType', 'Polygon') # Polygon or Point

    if "attention" in checkpoint:
        # Load the attention Unet model with custom objects for attention mechanisms
        logger.info(f"Loading model with attention from {checkpoint}")
        model = load_model(checkpoint, custom_objects={'multiplication': multiplication,
                                                            'multiplication2': multiplication2,
                                                            'dice_coef_loss':dice_coef_loss,
                                                            'dice_coef':dice_coef})
    else:
        logger.info(f"Loading standard model from {checkpoint}")
        # Load the standard Unet model with custom objects for dice coefficient loss
        model = load_model(checkpoint, custom_objects={'dice_coef_loss':dice_coef_loss, 
                                                            'dice_coef':dice_coef})
    
    logger.info(f"Model loaded from checkpoint {checkpoint}.")

    outputs = []
    for image_path in images_paths:
        logger.info(f"Loading image from {image_path}")
        with rasterio.open(image_path) as src:
            image = src.read()
        output = inference_image(image, image_path, legends, model, featureType)
        outputs.append(output)

    logger.info("Batch inference completed.")
    return outputs